%global _empty_manifest_terminate_build 0
%global pypi_name mitmproxy-wireguard
%global source_name mitmproxy_wireguard

Name:           python-%{pypi_name}
Version:        0.1.23
Release:        1
Summary:        WireGuard interface for mitmproxy.

License:        MIT
URL:            https://github.com/decathorpe/mitmproxy_wireguard
Source0:        %{url}/archive/%{version}/%{source_name}-%{version}.tar.gz
Source1:        cargo-vendor-cache
Patch1:         0001-set-cargo-vendor-cache-for-local-build.patch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-setuptools-rust
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:	python3-hatchling
BuildRequires:	python3-maturin
BuildRequires:	rust-packaging

%description
WireGuard interface for mitmproxy.


%package -n     python3-%{pypi_name}
Summary:        %{summary}

%description -n python3-%{pypi_name}
WireGuard interface for mitmproxy.


%prep
%autosetup -p1 -n %{source_name}-%{version}
tar xzvf %{SOURCE1} -C .

%build
%pyproject_build

%install
%pyproject_install

%files -n python3-%{pypi_name}
%doc README.md
%license LICENSE
%{python3_sitearch}/mitmproxy_wireguard
%{python3_sitearch}/mitmproxy_wireguard*.dist-info/

%changelog
* Tue Jun 27 2023 Dongxing Wang <dxwangk@isoftstone.com> - 0.1.23-1
- Initial package
